<?php

/*
Template Name: Price Template
 */

get_header();
?>
<!-- body start -->
<section class="wrapper default_body_wrapper clearfix">

	<!-- price title start -->
	<h2><?php the_title(); ?></h2><!-- price title end -->
	<!-- price article start -->
	<article class="price_box">
	<?php
	$testimonialsPost = new WP_Query('cat=13');

	if ($testimonialsPost->have_posts()) :
		while ($testimonialsPost->have_posts()) : $testimonialsPost->the_post();
	?>
		<!-- price label start -->
		<div class="price_label">
			<!-- room des start -->
			<div class="room"><?php the_content(); ?></div><!-- room des end -->

			<!-- room price start -->
			<div class="room_price">Start from <span class="price"><?php the_title(); ?></span> Incl.GST</div><!-- room price end -->
		</div>
		<!-- price label end -->
<?php
		endwhile;
	else :
		echo '<p>No content found</p>';
	endif;
	wp_reset_postdata();
?>
	</article><!-- price article end -->
<?php
	if (have_posts()) :
		while (have_posts()) : the_post();
?>
	<div class="price_content"><?php the_content(); ?></div>
<?php
		endwhile;
	else :
		echo '<p>No content found</p>';
	endif;
?>

</section><!-- body end -->
<?php get_footer(); ?>