<?php

/*
Template Name: Contact Us Template
 */

get_header();
?>

	<!-- body start -->
	<section class="wrapper default_body_wrapper clearfix">

		<!-- contact us title start -->
		<h2><?php  the_title(); ?></h2><!-- contacg us title end -->

		<!-- address start -->
		<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		<address>
			<?php the_content(); ?>
		</address><!-- address end -->
		<?php
			endwhile;

			else :
				echo '<p>No content found</p>';
			endif;

			wp_reset_postdata();
		?>
		<!-- contact us form start -->
		<div class="contact_form">
		<?php
		$testimonialsPost = new WP_Query('cat=14');

		if ($testimonialsPost->have_posts()) :
			while ($testimonialsPost->have_posts()) : $testimonialsPost->the_post();
		?>
			<?php the_content(); ?>
		<?php
			endwhile;

			else :
				echo '<p>No content found</p>';
			endif;

			wp_reset_postdata();
		?>
		</div><!-- contact us form end -->
	</section><!-- body end -->

<?php get_footer(); ?>