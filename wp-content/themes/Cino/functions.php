<?php
	// resousreces support
	function cino_css(){
		wp_enqueue_style('style', get_stylesheet_uri());
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'cino_css');

	// theme setup
	function cino_setup(){
		// navgation menu
		register_nav_menus(
			array(
				'primary' => __('Cino Menu')
			)
		);
		// add featured image support
		add_theme_support('post-thumbnails');
		add_image_size('home-thumbnail', 230, 200, true);
		add_image_size('gallery-thumbnail', 290, 190, ture);
		add_image_size('protfolio-thumbnail', 455, 290, ture);
		add_image_size('single-thumbnail', 150, 150, ture);
		// add post format support
		add_theme_support('post-formats', array('gallery','link'));
	}
	add_action('after_setup_theme', 'cino_setup');

	// add widget location
	function cino_widget(){
		register_sidebar( array(
			'name' => 'footerWidget',
			'id' => 'footer'
		));
	}
	add_action('widgets_init', 'cino_widget');
?>